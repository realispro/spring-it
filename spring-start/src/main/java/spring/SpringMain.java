package spring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import spring.config.SpringConfig;

import java.time.LocalDate;

public class SpringMain {

    public static void main(String[] args) {

        Person kowalski = new Person("Jan", "Kowalski",
                new Ticket(LocalDate.now().minusDays(0)));

        ApplicationContext context =
                //new ClassPathXmlApplicationContext("classpath:/context.xml");
                new AnnotationConfigApplicationContext(SpringConfig.class);

        Travel travel = context.getBean(Travel.class);
        travel.travel(kowalski);


        System.out.println("done.");
    }
}
