package spring.config;

import org.springframework.context.annotation.*;

import java.util.List;

@Configuration
@EnableAspectJAutoProxy
@ComponentScan(
        value="spring",
        excludeFilters = @ComponentScan.Filter(
                type= FilterType.ASPECTJ,
                pattern = "org.springframework.context.annotation"
        ))
public class SpringConfig {

    @Bean
    List<String> meals(){
        return List.of(
                "pomidorowa",
                "schabowy",
                "kompot"
        );
    }

    @Bean
    String name(){
        return "Winter holiday 2024";
    }

}
