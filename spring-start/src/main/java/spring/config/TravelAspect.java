package spring.config;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;
import spring.Person;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

@Aspect
@Component
public class TravelAspect {

    @Pointcut("execution(public * spring.impl..*(..))")
    private void allInImpl(){}

    @Pointcut("execution(public * spring..*.travel(..))")
    private void specificMethod(){}

    @Before("allInImpl()")
    void logEnteringMethod(JoinPoint jp){
        System.out.println("[ENTERING] " + jp.toString()
                + " on " + jp.getTarget().getClass().getSimpleName());
    }

    @After("allInImpl() || specificMethod()")
    void logExitingMethod(JoinPoint jp){
        System.out.println("[EXITING] " + jp.toString()
                + " on " + jp.getTarget().getClass().getSimpleName());
    }

    @Around("allInImpl()")
    Object logExecutionTime(ProceedingJoinPoint jp) throws Throwable {

        Object returned = null;
        try {
            LocalTime start = LocalTime.now();
            returned = jp.proceed(jp.getArgs()); // 1. replace args
            LocalTime end = LocalTime.now();
            System.out.println("[TIME] " + jp.toString() + " execution took "
                    + Duration.between(start, end).toMillis() + " millis");
        } catch (Throwable e) {
            // 2. exception handling
            throw e;
        }
        return returned; // 3. replace returned object
    }

    @Before("execution(public * *(spring.Person))")
    void ticketValidation(JoinPoint jp){
        Person person = (Person) jp.getArgs()[0];
        if(person.getTicket().getValid().isBefore(LocalDate.now())){
            throw new IllegalArgumentException("[TICKET VALIDATION FAILED]");
        } else {
            System.out.println("[TICKET VALIDATION SUCCEEDED] " + jp.toString());
        }
    }
}
