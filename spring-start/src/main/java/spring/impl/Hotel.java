package spring.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import spring.Accomodation;
import spring.Person;


import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Component
@PropertySource("/meals.properties")
public class Hotel implements Accomodation {

    private String gratis;

    private List<String> meals;

    //@Autowired
    @Value("${meal.gratis:nic}")
    public void setGratis(String gratis) {
        this.gratis = gratis;
    }

    //@Autowired
    //@Qualifier("meals")
    @Resource
    public void setMeals(List<String> meals) {
        this.meals = new ArrayList<>(meals);
    }

    @PostConstruct
    private void addGratis(){
        meals.add(gratis);
    }

    @Override
    public void host(Person p) {
        System.out.println("person " + p + " is being hosted in hotel. meal: " + meals);
    }
}
