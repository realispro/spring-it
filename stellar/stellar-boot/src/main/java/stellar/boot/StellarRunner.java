package stellar.boot;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import stellar.boot.repository.SystemRepository;

@Component
@Slf4j
@RequiredArgsConstructor
public class StellarRunner implements CommandLineRunner {

    private final SystemRepository repository;

    @Override
    public void run(String... args) throws Exception {
        log.info("stellar runner started.");
        repository.findAll().forEach(s->log.info("planetary system: {}", s));
    }
}
