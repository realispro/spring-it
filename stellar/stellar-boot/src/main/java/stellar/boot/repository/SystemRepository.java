package stellar.boot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import stellar.boot.model.PlanetarySystem;

import java.util.List;

public interface SystemRepository extends JpaRepository<PlanetarySystem, Integer> {
    List<PlanetarySystem> findAllByNameContaining(String phrase);
}
