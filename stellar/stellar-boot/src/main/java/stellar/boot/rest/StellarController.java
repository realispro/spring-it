package stellar.boot.rest;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.i18n.LocaleContextResolver;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import stellar.boot.model.PlanetarySystem;
import stellar.boot.repository.PlanetRepository;
import stellar.boot.repository.SystemRepository;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/stellar/webapi")
public class StellarController {

    private final SystemRepository systemRepository;
    private final PlanetRepository planetRepository;
    private final MessageSource messageSource;
    private final LocaleResolver localeResolver;

    @GetMapping("/systems")
    @PreAuthorize("hasRole('REGULAR')")
    List<PlanetarySystem> getSystems(@RequestParam(value = "phrase", required = false) String phrase){
        log.info("about to search systems using phrase: {}", phrase);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        log.info("authenticated user: {}", authentication);
        return phrase==null
                ? systemRepository.findAll()
                : systemRepository.findAllByNameContaining(phrase);
    }

    @PostMapping("/systems")
    ResponseEntity<?> addSystem(
            HttpServletRequest request,
            @Validated @RequestBody PlanetarySystem system, Errors errors){
        log.info("about to add new system: {}", system);

        if(errors.hasErrors()){
            String message = errors.getAllErrors().stream()
                    .map(oe->messageSource.getMessage(oe.getCode(), new Object[0], localeResolver.resolveLocale(request)))
                    .reduce("", (accu, code)-> accu + code + "\n");
            return ResponseEntity.badRequest().body(message);
        }

        system = systemRepository.save(system);
        return ResponseEntity.created(ServletUriComponentsBuilder
                .fromCurrentRequestUri()
                .path("/" + system.getId())
                .build()
                .toUri()).body(system);

    }



}
