package stellar.dao.jdbc;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import stellar.dao.PlanetDAO;
import stellar.model.Planet;
import stellar.model.PlanetarySystem;

import javax.sql.DataSource;
import java.util.List;
import java.util.logging.Logger;

@Repository
public class JDBCPlanetsDAO implements PlanetDAO {

    public static final Logger logger = Logger.getLogger(JDBCPlanetsDAO.class.getName());

    public static final String SELECT_ALL_PLANETS = "select p.id as planet_id, p.name as planet_name, " +
            "p.size as planet_size, p.weight as planet_weight, p.moons as planet_moons " +
            "from planet p";

    public static final String SELECT_PLANETS_BY_SYSTEM = "select p.id as planet_id, p.name as planet_name, " +
            "p.size as planet_size, p.weight as planet_weight, p.moons as planet_moons " +
            "from planet p where system_id=?";

    public static final String SELECT_PLANETS_BY_SYSTEM_AND_NAME = "select p.id as planet_id, p.name as planet_name, " +
            "p.size as planet_size, p.weight as planet_weight, p.moons as planet_moons " +
            "from planet p where system_id=? and name like ?";


    private final JdbcTemplate jdbcTemplate;

    public JDBCPlanetsDAO(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);

    }

    @Override
    public List<Planet> getAllPlanets() {
        return jdbcTemplate.query(SELECT_ALL_PLANETS, new PlanetRowMapper());
    }

    @Override
    public List<Planet> getPlanetsBySystem(PlanetarySystem system) {
        return jdbcTemplate.query(SELECT_PLANETS_BY_SYSTEM, new PlanetRowMapper(), system.getId());
    }

    @Override
    public List<Planet> getPlanetsBySystemAndName(PlanetarySystem system, String like) {
        return null;
    }

    @Override
    public Planet getPlanetById(int id) {
        return null;
    }

    @Override
    public Planet addPlanet(Planet p) {
        return null;
    }


}
