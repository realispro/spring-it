package stellar.dao.jpa;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
import stellar.dao.SystemDAO;
import stellar.model.PlanetarySystem;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class JPASystemDAO implements SystemDAO {

    @PersistenceContext(unitName = "stellar")
    private EntityManager em;

    @Override
    public List<PlanetarySystem> getAllPlanetarySystems() {
        // JPQL -> HQL -> SQL
        return em.createQuery("select s from PlanetarySystem s")
                .getResultList();
    }

    @Override
    public List<PlanetarySystem> getPlanetarySystemsByName(String like) {
        return em.createQuery("select s from PlanetarySystem s where s.name like :like")
                .setParameter("like", "%" + like + "%")
                .getResultList();
    }

    @Override
    public PlanetarySystem getPlanetarySystem(int id) {
        return em.find(PlanetarySystem.class, id);
    }

    @Override
    public PlanetarySystem addPlanetarySystem(PlanetarySystem system) {
        em.persist(system);
        return system;
    }
}
