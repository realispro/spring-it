package stellar.dao.mem;



import stellar.model.Planet;
import stellar.model.PlanetarySystem;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class InMemory {

    static List<PlanetarySystem> systems = new ArrayList<>();

    static {
        PlanetarySystem s = createPlanetarySystem(
                1,
                "Solar System",
                "Sun",
                java.sql.Date.valueOf(LocalDate.of(857, 3, 4)),
                3.234f,
                "https://en.wikipedia.org/wiki/Solar_System");
        s.setPlanets(Arrays.asList(
                createPlanet(1, "Mercury", 2439, 3, 0, s),
                createPlanet(2, "Venus", 6051, 5, 0, s),
                createPlanet(3, "Earth", 6371, 60, 1, s),
                createPlanet(4, "Mars", 3389, 6, 2, s),
                createPlanet(5, "Jupiter", 69911, 10000, 79, s),
                createPlanet(6, "Neptun", 24622, 1000, 14, s),
                createPlanet(7, "Saturn", 58232, 5000, 82, s),
                createPlanet(8, "Uranus", 25362, 800, 27, s)
        ));
        systems.add(s);
        s = createPlanetarySystem(
                2,
                "Kepler-90 System",
                "Kepler-90",
                java.sql.Date.valueOf(LocalDate.of(1987, 11, 2)),
                12.275123f,
                "https://pl.wikipedia.org/wiki/Kepler-90");
        s.setPlanets(Arrays.asList(
                createPlanet(21, "b", 0,0,0, s),
                createPlanet(22, "c", 0,0,0, s),
                createPlanet(23, "i", 0,0,0, s),
                createPlanet(24, "d", 0,0,0, s),
                createPlanet(25, "e", 0,0,0, s),
                createPlanet(26, "f", 0,0,0, s),
                createPlanet(27, "g", 0,0,0, s)
        ));
        systems.add(s);
        s = createPlanetarySystem(
                3,
                "Trappist-1 System",
                "Trappist-1",
                java.sql.Date.valueOf(LocalDate.of(2013, 7, 27)),
                7.62946f,
                "https://pl.wikipedia.org/wiki/TRAPPIST-1");
        s.setPlanets(Arrays.asList(
                createPlanet(31, "b", 0,0,0, s),
                createPlanet(32, "c", 0,0,0, s),
                createPlanet(33, "d", 0,0,0, s),
                createPlanet(34, "e", 0,0,0, s),
                createPlanet(35, "f", 0,0,0, s),
                createPlanet(36, "g", 0,0,0, s),
                createPlanet(37, "h", 0,0,0, s)
        ));
        systems.add(s);
    }


    private static PlanetarySystem createPlanetarySystem(int id, String name, String star, Date discovery, float distance, String url) {
        PlanetarySystem ps = new PlanetarySystem();
        ps.setId(id);
        ps.setName(name);
        ps.setStar(star);
        ps.setDiscovery(discovery);
        ps.setDistance(distance);
        return ps;
    }

    private static Planet createPlanet(int id, String name, int size, int weight, int moons, PlanetarySystem system) {
        Planet planet = new Planet();
        planet.setId(id);
        planet.setName(name);
        planet.setSize(size);
        planet.setWeight(weight);
        planet.setMoons(moons);
        planet.setSystem(system);
        return planet;
    }


}
