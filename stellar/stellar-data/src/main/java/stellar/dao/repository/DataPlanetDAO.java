package stellar.dao.repository;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
import stellar.dao.PlanetDAO;
import stellar.model.Planet;
import stellar.model.PlanetarySystem;

import java.util.List;

@Repository
@Primary
public class DataPlanetDAO implements PlanetDAO {

    private final PlanetRepository repository;

    public DataPlanetDAO(PlanetRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Planet> getAllPlanets() {
        return repository.findAll();
    }

    @Override
    public List<Planet> getPlanetsBySystem(PlanetarySystem system) {
        return repository.findAllBySystem(system);
    }

    @Override
    public List<Planet> getPlanetsBySystemAndName(PlanetarySystem system, String like) {
        return repository.findAllBySystemAndNameContaining(system, like);
    }

    @Override
    public Planet getPlanetById(int id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public Planet addPlanet(Planet p) {
        return repository.save(p);
    }
}
