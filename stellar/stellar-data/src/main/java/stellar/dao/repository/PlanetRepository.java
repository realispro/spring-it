package stellar.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import stellar.model.Planet;
import stellar.model.PlanetarySystem;

import java.util.List;

public interface PlanetRepository extends JpaRepository<Planet, Integer> {

    @Query(value = "select p from Planet p where p.system=:system")
    List<Planet> findAllBySystem(@Param("system") PlanetarySystem system);

    List<Planet> findAllBySystemAndNameContaining(PlanetarySystem system, String name);

}
