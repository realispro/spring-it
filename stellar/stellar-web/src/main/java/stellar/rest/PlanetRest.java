package stellar.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import stellar.model.Planet;
import stellar.model.PlanetarySystem;
import stellar.service.StellarService;

import java.util.List;

@RestController
@RequestMapping("/webapi")
public class PlanetRest {

    private final StellarService service;

    public PlanetRest(StellarService service) {
        this.service = service;
    }

    @GetMapping("/planets")
    List<Planet> getPlanets(){
        return service.getPlanets();
    }

    @GetMapping("/planets/{id}")
    ResponseEntity<Planet> getSystem(@PathVariable("id") int id){
        Planet planet = service.getPlanetById(id);
        if(planet!=null) {
            return ResponseEntity.status(HttpStatus.OK).body(planet);
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
