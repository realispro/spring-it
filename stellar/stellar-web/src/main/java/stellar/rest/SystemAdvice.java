package stellar.rest;

import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.InitBinder;

@ControllerAdvice
public class SystemAdvice {

    private final SystemValidator validator;

    public SystemAdvice(SystemValidator validator) {
        this.validator = validator;
    }

    @InitBinder
    void initBinder(WebDataBinder binder){
        binder.setValidator(validator);
    }
}
