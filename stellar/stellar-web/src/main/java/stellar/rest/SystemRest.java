package stellar.rest;

import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.LocaleResolver;
import stellar.model.Planet;
import stellar.model.PlanetarySystem;
import stellar.service.StellarService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

@RestController
@RequestMapping("/webapi/systems")
public class SystemRest {

    private static final Logger logger = Logger.getLogger(SystemRest.class.getName());

    private final StellarService service;
    private final MessageSource messageSource;
    private final LocaleResolver localeResolver;

    public SystemRest(StellarService service, MessageSource messageSource, LocaleResolver localeResolver) {
        this.service = service;
        this.messageSource = messageSource;
        this.localeResolver = localeResolver;
    }


    @GetMapping // /systems?phrase=abc
    List<PlanetarySystem> getSystems(
            @RequestParam(value = "phrase", required = false) String phrase,
            @RequestHeader(value = "User-Agent", required = false) String userAgent) {
        logger.info("searching for a systems using phrase: " + phrase);
        logger.info("User-Agent: " + userAgent);

        if(phrase!=null&&phrase.equals("foo")){
            throw new IllegalArgumentException("Foo!");
        }


        return phrase==null
                ? service.getSystems()
                : service.getSystemsByName(phrase);
    }

    @GetMapping("/{id}")
    ResponseEntity<PlanetarySystem> getSystem(@PathVariable("id") int id) {

        //ResponseEntity.of(java.util.Optional.ofNullable(service.getSystemById(id)));

        PlanetarySystem system = service.getSystemById(id);
        if (system != null) {
            return ResponseEntity.status(HttpStatus.OK).body(system);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/{systemId}/planets")
    ResponseEntity<List<Planet>> getPlanetsBySystem(@PathVariable("systemId") int systemId) {

        PlanetarySystem system = service.getSystemById(systemId);
        if (system == null) {
            return ResponseEntity.notFound().build();
        }

        List<Planet> planets = service.getPlanets(system);
        return ResponseEntity.ok(planets);

    }

    @PostMapping
    ResponseEntity<?> addSystem(
            HttpServletRequest request,
            @Validated @RequestBody PlanetarySystem system,
            Errors errors) {

        logger.info("about to add new planetary system: " + system.toString());

        if (errors.hasErrors()) {
            Locale locale = localeResolver.resolveLocale(request);

            String errorMessage = errors.getAllErrors()
                    .stream()
                    .map(oe ->
                            messageSource.getMessage(
                                    oe.getCode(),
                                    oe.getArguments(),
                                    locale))
                    .reduce("errors: \n", (accu, code) -> accu + code + "\n");
            return ResponseEntity.badRequest().body(errorMessage);
        }

        system = service.addPlanetarySystem(system);
        return ResponseEntity.status(HttpStatus.CREATED).body(system);

    }

    @ExceptionHandler(IllegalArgumentException.class)
    ResponseEntity<?> handleIllegalArgumentException(IllegalArgumentException e){
        logger.log(Level.SEVERE, "Houston, we have a problem!", e);
        return ResponseEntity.status(HttpStatus.I_AM_A_TEAPOT).body(e.getMessage());
    }

    @ExceptionHandler(Exception.class)
    ResponseEntity<?> handleException(Exception e){
        logger.log(Level.SEVERE, "Houston, we have general problem!", e);
        return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
    }

}
