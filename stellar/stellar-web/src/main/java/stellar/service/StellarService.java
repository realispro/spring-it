package stellar.service;

import stellar.model.Planet;
import stellar.model.PlanetarySystem;

import java.util.List;

public interface StellarService {

    List<PlanetarySystem> getSystems();

    List<PlanetarySystem> getSystemsByName(String like);

    PlanetarySystem getSystemById(int id);

    List<Planet> getPlanets(PlanetarySystem s);

    public List<Planet> getPlanets(PlanetarySystem system, String like);

    Planet getPlanetById(int id);

    Planet addPlanet(Planet p, PlanetarySystem s);

    PlanetarySystem addPlanetarySystem(PlanetarySystem s);

    List<Planet> getPlanets();
}
