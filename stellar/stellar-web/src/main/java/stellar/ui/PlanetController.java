package stellar.ui;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import stellar.model.Planet;
import stellar.model.PlanetarySystem;
import stellar.service.StellarService;

import java.util.List;

import static java.util.Collections.singletonList;

@Controller
public class PlanetController {

    private final StellarService stellarService;

    public PlanetController(StellarService stellarService) {
        this.stellarService = stellarService;
    }

    @GetMapping("/planets")
    String getPlanet(@RequestParam(name="systemId") int id, Model model) {
        PlanetarySystem system = stellarService.getSystemById(id);

        if (system == null) {
            return "error";
        }

        List<Planet> planets = stellarService.getPlanets(system);

        model.addAttribute("system", system);
        model.addAttribute("planets", planets);

        return "planets";
    }
}
