package stellar.ui;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import stellar.model.PlanetarySystem;
import stellar.service.StellarService;

import java.util.Date;
import java.util.List;

@Controller
//@RequestMapping("/systems")
public class SystemController {

    private final StellarService service;

    public SystemController(StellarService service) {
        this.service = service;
    }

    @GetMapping("/systems")
    String getSystems(
            Model model,
            @RequestParam(value = "phrase", required = false) String phrase){
        List<PlanetarySystem> systems =  phrase==null
                ? service.getSystems()
                : service.getSystemsByName(phrase);

        model.addAttribute("systems", systems);

        return "systems";
    }

    @GetMapping("/addSystem")
    String prepareAddingSystem(Model model){
        PlanetarySystem system = new PlanetarySystem();
        system.setDiscovery(new Date());
        system.setName("New System");
        model.addAttribute("systemForm", system);

        return "addSystem";
    }

    @PostMapping("/addSystem")
    String addSystem(@Validated @ModelAttribute("systemForm") PlanetarySystem system, Errors errors){

        if(errors.hasErrors()){
            return "addSystem";
        }

        system = service.addPlanetarySystem(system);

        return "redirect:/systems";
    }

}
